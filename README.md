# mvi-sp

## Google Landmark Recognition

Do you sometimes wonder what it is behind you in that photo, what’s that temple or mountain?  This technology could predict landmark labels to help you better organize your photo albums, or just to let you be aware of what it is in your photo. Thos topis refers to Kaggle competition here -> https://www.kaggle.com/competitions/landmark-recognition-2021/data.

Google Landmark Recognition should predict landmark labels directly from image pixels. Landmark recognition should not only assign the correct classification, it should be also capable of defining of what landmark is and what not. However, in this project I will try to keep it simpler and train the CNN only to recognize correct classification of image. 

## Dataset 

Data are downloaded from competition on Kaggle website, where they are already divided into training and test set.

